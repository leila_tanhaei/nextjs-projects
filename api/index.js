import axios from 'axios';

export const getCategories = () =>  {
    return(
        axios.get('https://catapi-hamidtanhaei.vercel.app/categories', {})
    )
}

export const getPictures = ({params}) =>  {
    return axios({
        url: 'https://catapi-hamidtanhaei.vercel.app/images/search',
        params,
    })
}

