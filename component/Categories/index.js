import Link from 'next/link';
import { useContext } from 'react';
import { CategoriesContext } from '../../store/categories';


function Categories() {
  const {loading, categories} = useContext(CategoriesContext)
  return(
    <div className="list">
      <h1>the cats</h1>
      {loading && "loading..."}
      {<div>
        <ul>
          {categories.map(post => 
            <li key={post.id}>
              <Link href={"/categories/"+post.id}>
                <a>title: {post.name}</a>
              </Link>
              <img/>
            </li>
          )}
        </ul>
      </div>}
    </div>
  )
}

export default Categories;