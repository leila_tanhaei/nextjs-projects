import React from "react";
import Categories from "../Categories/index";

function MainLayout({children}) {
    return(
        <div className="layout">
            <Categories/>
            {children}
        </div>
    )
}

export default MainLayout;