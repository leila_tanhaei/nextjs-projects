import React from 'react';
import axios from 'axios';
import CategoriesContainer from "../store/categories";
import MainLayout from "../component/layout/MainLayout";

export default function App({ Component, pageProps }) {
    axios.defaults.headers.common['x-api-key'] = "086a9087-b28c-4f49-be5d-26d5aff48886";
    
    return (
        <CategoriesContainer>
            <MainLayout>
                <Component {...pageProps} />
            </MainLayout>
        </CategoriesContainer>
    )
}