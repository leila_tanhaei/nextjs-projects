import React from "react";
import { getCategories, getPictures } from "../../api";


function CatImages({post}) {
    return(
        <div className="pictures">
            cats images
            <ul>
                {post.map(post => 
                    <li key={post.id}>
                        <img src={post.url}/>
                    </li>
                )}
            </ul>
        </div>
    )
}

export async function getStaticProps({params}) {
    try{
        const categoryId = params.id
        const response = await getPictures({params: {category_ids: categoryId, limit:10 }})
        return{
            props : {
                post: response.data,
            }
        }
    } catch(error) {
        console.error(error)
    }
}

export async function getStaticPaths() {
    try{
        const response = await getCategories()
        const paths = response.data.map(category => `/categories/${category.id}`)
        return{
            paths,
            fallback: false
        }
    } catch(error){
        console.error(error)
    }
}

export default CatImages;