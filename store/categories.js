import React, {useState, useEffect} from "react";
import { getCategories } from "../api";

export const CategoriesContext = React.createContext();

function CategoriesContainer({children}) {
    const [loading, setLoading] = useState(true);
    const [categoriesList, setCategoriesList] = useState([]);
    useEffect(() => {
        getCategories().then((res) => {
            setCategoriesList(res.data);
            setLoading(false)
        });
    }, []);
    return (
      <CategoriesContext.Provider value={{loading, categories: categoriesList}}>
        {children}
      </CategoriesContext.Provider>
    );
  }

  export default CategoriesContainer;